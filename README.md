![](https://i.imgur.com/8o8LLOu.png)
## Fast access to your playlists

**Usage:**
* **`playlist`** `<playlist number>`

**Configs:**
* All playlists saved in `/etc/playlist-path`

You can add your playlist as folder *paths* in local storage or as *http\* links* (e.g. `/home/user/music/my-favourite-music`, `https://www.youtube.com/watch?v=dQw4w9WgXcQ`)
