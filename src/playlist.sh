#!/bin/bash
paths=/etc/playlist-path
count=$(cat $paths | wc -l)

if [ $count -eq 0 ]
then
	notify-send 'There is no playlists specified.'
	exit 1
fi

if [ $# -eq 0 -o $1 -gt $count ]
then
	path=$(sed -n 1p $paths)
else
	path=$(sed -n "$1p" $paths)
fi

if [ -e $path ]
then
	mpv --shuffle $path
else
	if [ "$path" = "http*" ]
	then
		mpv $path
	else
		notify-send 'No such directory:' "$path"
	fi
fi
